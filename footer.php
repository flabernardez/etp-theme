
<footer id="colophon" class="site-footer">

	<article class="wrapper">

		<section class="footer-site-name">
            <?php

            $url = get_site_url( null, "", "https");
		    echo str_replace( 'https://', ' ', $url);

		    ?>
        </section>

        <?php
		if ( is_active_sidebar( 'footer-widget' ) ) : ?>

            <section class="footer-widget-area">
				<?php dynamic_sidebar( 'footer-widget' ); ?>
			</section>

		<?php
		endif; ?>

		<nav class="footer-navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'footer-menu',
			) );
			?>
		</nav><!-- #site-navigation -->

	</article>

</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
