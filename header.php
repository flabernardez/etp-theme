<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php

    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

?>
<header id="masthead" class="site-header" style="
        background-image: linear-gradient(135deg, rgba(48,83,89,0.49) 0%, rgba(30,44,46,0.67) 100%),
        url('<?php echo $thumb['0']; ?>');">

	<section class="top-bar">

		<article class="wrapper">

			<section class="site-title">
				<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				$url = home_url( '/', 'https' );
				if ( has_custom_logo() ) {
					echo '<a href="'. esc_url( $url ) .'" title="logo inicio"><img src="'. esc_url( $logo[0] ) .'" alt="logo ' . get_bloginfo( 'name' ) . '" title="logo ' . get_bloginfo( 'name' ) . '"></a>';
				} else {
					echo '<a href="'. esc_url( $url ) .'" title="logo inicio">'. get_bloginfo( 'name' ) .'</a>';
				}
				?>
			</section>
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false" aria-label="Menu"></button>
                <?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</nav><!-- #site-navigation -->

		</article>

	</section><!-- .top-bar -->
	<section class="hero">

		<?php
		if  ( is_home() ) : ?>

			<h1> <?php esc_html_e( 'All Articles', 'cancer-theme' ) ?></h1>

		<?php
        elseif ( is_archive() ) :

            echo '<h1>' . get_the_archive_title() . '</h1>';
            echo '<p class="excerpt">' . get_the_archive_description() . '</p>';

        elseif ( is_404() ) : ?>

            <h1> <?php esc_html_e( 'Error 404', 'cancer-theme' ) ?></h1>

		<?php
        elseif ( is_search() ) : ?>

            <h1> <?php esc_html_e( 'Search results for:', 'cancer-theme' );?> <?php echo get_search_query(); ?></h1>

        <?php
        elseif ( is_front_page() || is_singular('numero') ) : ?>

            <h1><?php the_title(); ?></h1>

            <section class="meta">

				<?php

				if( get_field('numero_etp') ) {
					echo '<p class="tag">' . __( 'ETP number ', 'cancer-theme' ) . get_field('numero_etp') . '</p>';
				}
				?>

                <date> <?php esc_html_e( 'Publish date ', 'cancer-theme' ) . the_date() ?></date>

            </section>

		<?php

        elseif ( is_single() ) : ?>

            <h1><?php the_title(); ?></h1>

            <section class="meta">

                <div> <?php the_author(); ?></div>

            </section>


	        <?php

	        if ( get_the_excerpt() ) {
		        echo '<p class="excerpt">' . get_the_excerpt() . '</p>';
	        }

	        ?>

        <?php
		else : ?>

            <h1> <?php the_title() ?></h1>

		<?php
		endif; ?>

	</section>

</header><!-- #masthead -->

