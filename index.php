<?php

get_header(); ?>

    <main id="content" class="site-content">

	    <?php
	    if ( have_posts() ) :

		    /* Start the Loop */
		    while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <section class="entry-content">

		                <?php

		                the_content( sprintf(
			                wp_kses(
			                /* translators: %s: Name of current post. Only visible to screen readers */
				                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'cancer-theme' ),
				                array(
					                'span' => array(
						                'class' => array(),
					                ),
				                )
			                ),
			                get_the_title()
		                ) );

                        if ( is_singular('post') ) :

                            if ( is_active_sidebar( 'post-widget' ) ) : ?>

                                <section class="post-widget-area">
                                    <?php dynamic_sidebar( 'post-widget' ); ?>
                                </section>

	                        <?php
	                        endif;

                            if ( comments_open() || get_comments_number() ) :

                                comments_template();

                            endif;

                            // Display related posts of current post
                            if ( function_exists( 'rp4wp_children' ) ) :

                                rp4wp_children();

                            endif;

	                    endif;

		                wp_link_pages( array(
			                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cancer-theme' ),
			                'after'  => '</div>',
		                ) );

		                ?>

                    </section><!-- .entry-content -->

                </article><!-- #post-<?php the_ID(); ?> -->

		    <?php

            endwhile;

		    the_posts_navigation();

	    else :

		    get_template_part( 'template-parts/content', 'none' );

	    endif;

	    ?>

    </main><!-- #content -->


<?php

get_footer(); ?>